# SSTat Du simpla

Pronounced S-stat. Yet another potentiostat based on Arduino board.

SSTat *Du simpla* is a bipotentiostat of which both working electrodes are potentiostated at the same voltage.

Status: In development. Preparing for the release of its first version.

* *master* branch contains **ONLY** stable releases.
* *development* branch contains the lastest changes to be incorporated eventualy at the *master* branch.


# Download

Currently there are no stable files, therefore, to install the lastest software, you have to download the lastest files in *development* branch. To do so:

1. Change from *master* to the *development* branch by clicking in the selection box located above the list of files, at the left-hand side.

2. Download the files by clicking the *download icon* located above the list of files, at the right-hand side. You can select among different compressed files: .zip, .tar.gz, etc.


# Installation

## Firmware

Use the following steps to install the firmware in your *Arduino Due*

1. First make sure that you can [use your Arduino Due on the Arduino Desktop IDE](https://www.arduino.cc/en/Guide/ArduinoDue#toc2)

2. Make sure you have installed the *DueTimer* library. You can do so by using the *Library manager* at the *Arduino IDE*

3. Upload the firmware using the *Arduino IDE*.

### Troubleshooting

#### Uploading firmware: No device found on ttyACM0

[](http://forum.arduino.cc/index.php?topic=148613.msg1117379#msg1117379)

From GNU/Linux terminal:
```
$ stty -F /dev/ttyACM0 speed 1200 cs8 -cstopb -parenb
$ /opt/arduino-1.8.5/arduino &
```
run it a couple of times until the baudrate changes to 1200

From Windows command prompt:
```
mode com3: baud=1200 data=8 parity=n stop=1
```
Apparently, this procedure must be done every time a sketch is uploaded.

## GUI

### Requirements

Unless using the `.exe` version for Windows, the GUI requires *python* and the following libraries/packages:

1. [PyQtGraph](http://pyqtgraph.org/), and its [dependencies](https://github.com/pyqtgraph/pyqtgraph#requirements)
2. [NumPy](http://www.numpy.org/)
3. [time](https://docs.python.org/2/library/time.html)
4. [pySerial](http://pythonhosted.org/pyserial/)
5. sys
6. glob

